        // Теоретичні питання
        
// 1. Які існують типи даних у Javascript ?

//         У Javascript існує 8 типів даних: число, рядок, boolean, null, undefined, об'єкт, 
//              символ, bigint.

// 2. У чому різниця між == і ===?
        
//         == - нестрогий оператор рівності - виконує перетворення типів порівнюваих значень 
//              до числа, що може призводити до помилок.
//         === - строгий оператор рівності - не приводить до одного типу даних.

// 3. Що таке оператор ?
        
//         Знак операції, такий як наприклад  +,-,*,/ , називають оператором.
//         Оператори виконують операції над певними значеннями, які називаються операндами.
                
                
    
    //     Завдання
 
/*Отримати за допомогою модального вікна браузера дані користувача: ім'я та вік.
Якщо вік менше 18 років - показати на екрані повідомлення:
You are not allowed to visit this website.
Якщо вік від 18 до 22 років(включно) – показати вікно з наступним повідомленням:
Are you sure you want to continue? і кнопками Ok, Cancel.
Якщо користувач натиснув Ok, показати на екрані повідомлення: Welcome, + ім'я користувача.
Якщо користувач натиснув Cancel, показати на екрані повідомлення:
You are not allowed to visit this website.
Якщо вік більше 22 років – показати на екрані повідомлення: Welcome, + ім'я користувача.
Обов'язково необхідно використовувати синтаксис ES6 (ES2015) для створення змінних.
Після введення даних додати перевірку їхньої коректності.
Якщо користувач не ввів ім'я, або при введенні віку вказав не число -
запитати ім'я та вік наново(при цьому дефолтним значенням для кожної зі змінних має бути
введена раніше інформація).*/


let userName = prompt("Enter your name:");
let userAge = parseInt(prompt("Enter your age:"));


if (!userName || isNaN(userAge)) {
  userName = prompt("Enter your name again:", userName);
  userAge = parseInt(prompt("Enter your age again:", userAge));
}

if (userAge < 18) {
  alert("You are not allowed to visit this website.");
} else if (userAge >= 18 && userAge <= 22) {
        if (confirm('Are you sure you want to continue?') === true) {
                alert(`Welcome, ${userName}`)
        }
        else {
                alert('You are not allowed to visit this website')
        }
} else {
  alert(`Welcome, ${userName}`);
};